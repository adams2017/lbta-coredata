//
//  EmployeesController.swift
//  lbta-coredata
//
//  Created by Admin on 5/27/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit
import CoreData

class EmployeesController:UITableViewController {
    var company:Company?
    var arrEmployee = [Employee]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    #warning("used viewWillAppear for title")
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "\(company?.name ?? "<Unknown>" ) List"
        #warning("Fetch employees here, so when ever view appears it is current")
        fetchEmployees()
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
    }
    
    func setup() {
        tableView.backgroundColor = .red
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add Emp", style: .plain, target: self, action: #selector(addSelector))
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    @objc private func addSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) @ Line \(#line) of \(file)\n")
        let vc = CreateEmployeeController()
        vc.aCompany = company
        let nc = UINavigationController(rootViewController: vc)
        present(nc, animated: true, completion: nil)
    }
    
    func fetchEmployees() {
        
        arrEmployee = company?.employees?.allObjects as! [Employee] 
//        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
//        let fr = NSFetchRequest<Employee>(entityName: "Employee")
//
//        do {
//            self.arrEmployee = try CoreDataService.context.fetch(fr)
//            let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
//            _=arrEmployee.map { print($0.name as Any)}
//        } catch let err {
//            print("Error: \(err)")
//        }
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEmployee.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let aEmployee = arrEmployee[indexPath.row]
        let name = aEmployee.name
        let taxID = aEmployee.relationEmployeeInformation?.taxid
        let pos = aEmployee.position
        c.textLabel?.text = "\(name ?? "<no name>") \(pos ?? "no position")"
        c.textLabel?.textColor = UIColor.white
        c.backgroundColor = .blue
        c.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return c
    }
}
