//
//  Extensions.swift
//  lbta-coredata
//
//  Created by Admin on 5/17/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

import UIKit
extension UIColor {
    
    struct  ThemeStandard{
        static var navBar = UIColor(red: 247/255, green: 66/255, blue: 82/255, alpha: 1)
        static var tableView = UIColor(red: 9/255, green: 45/255, blue: 64/255, alpha: 1)
    }
    
    struct  ThemeWinter{
        static var navBar = UIColor(red: 247/255, green: 66/255, blue: 82/255, alpha: 1)
        static var tableView = UIColor(red: 9/255, green: 45/255, blue: 64/255, alpha: 1)
    }
}

extension UIColor {
   
    /*
    enum Tyre {
        
        enum TyreColor {
            case purple // UltraSoft
            case red // SuperSoft
            case yellow // Soft
            case white // Medium
            case orange // Hard
            case green // Intermediate
            case blue // FullWet
        }
        
        case ultraSoft
        case superSoft
        case soft
        case medium
        case hard
        case intermediate(litresClearedPerSecond: UInt8)
        case fullWet(litresClearedPerSecond: UInt8)
      
        // it has a variable that  must  be a TyroColor
        var tyreColor : TyreColor {
            get {
                switch self {
                case .ultraSoft: return .purple
                case .superSoft: return .red
                case .soft: return .yellow
                case .medium: return .white
                case .hard: return .orange
                case .intermediate: return .green
                case .fullWet: return .blue
                }
            }
        }
 */
// i want to be able to say UIColor.Theme.springtime.nav
    
    enum Th {
        enum Element {
            case navBar
            case tableView
        }
        
        case spring
        case summer
        case fall
        case winter
        

        
        
        
        /*
        var uiElement:UIElement {
            
            get {
                switch self {
                case .navBarreturn: return UIColor.red// UIColor.red
                case .tableView: return UIColor.white  // return UIColor.green
            }
          }
        }
   */
        
    
    
    
    }
    
    
    
    
}

enum Theme {
    case summer
    case winter
    //passed to the function, one per ui element to be styled
    enum Element {
        case header
        case footer
    }
    func color(for element:Element) -> UIColor {
        switch self {
        case .summer:
            switch element {
            case  .header: return UIColor.red
            case  .footer: return UIColor.blue
            }
        case .winter:
            switch element {
            case .header: return UIColor.green
            case .footer: return UIColor.orange
            }
        }
    }
}
