//
//  CompaniesController+CreateCompanyDelegate.swift
//  lbta-coredata
//
//  Created by Admin on 5/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation

extension CompaniesController:CreateCompanyDelegate  {
    
    func addCompany(_ company: Company) {
        companies.append(company)
        let indexPath = IndexPath(row: companies.count-1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func didEditCompany( _ company: Company ){
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function)   \(file) Line \(#line) \n")
        // update the table view here . need to get the index of the company changed
        // its been in the array the whole itme, so no need to add it
        //force the row containing the company parameter to reload itself
        guard  let ix = companies.firstIndex(of: company) else {
            fatalError("cant find index of company")
        }
        let ip = IndexPath(row: ix, section: 0)
        tableView.reloadRows(at: [ip], with: .middle)
    }
    
    
    
    
}
