//
//  AppDelegate.swift
//  lbta-coredata
//
//  Created by Admin on 5/17/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class LightStatusNC:UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
       return .lightContent
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var theme:Theme = Theme.winter
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = window ?? UIWindow()
        window?.makeKeyAndVisible()
        let rootVC = CompaniesController()
        let nc = LightStatusNC(rootViewController: rootVC)
        window?.rootViewController = nc
        setupNavigationBarStyle()
        
        return true
    }
  
    
    
    //for AppDelegate
    func setupNavigationBarStyle() {
        #warning("snippet")
    UINavigationBar.appearance().barTintColor = UIColor.ThemeStandard.navBar
    UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().isTranslucent = false
    UINavigationBar.appearance().prefersLargeTitles = true
    UINavigationBar.appearance().largeTitleTextAttributes = [
        .foregroundColor : UIColor.white]
    UINavigationBar.appearance().titleTextAttributes = [
        .foregroundColor : UIColor.white
    ]
    }
}

