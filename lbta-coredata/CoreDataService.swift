//
//  CoreDataManager.swift
//  lbta-coredata
//
//  Created by Admin on 5/20/19.
//  Copyright © 2019 AX Software. All rights reserved.
//
import CoreData
#warning("implement delete all ")
class CoreDataService {
    static let  shared = CoreDataService()
    let persistentContainer:NSPersistentContainer = {
        let pc =  NSPersistentContainer(name: "MyModels")
        pc.loadPersistentStores { (storeDesc, err) in
            if let err = err {
                fatalError("\(err)")
            }
        }
        return pc
    }()
   static var context:NSManagedObjectContext {
        get {
            return self.shared.persistentContainer.viewContext
        }
    }
    func fetchCompanies() -> Array<Company> {
        var arrCompany = Array<Company>()
        let fetchRequest = NSFetchRequest<Company>(entityName: "Company")
        do {
            arrCompany = try CoreDataService.context.fetch(fetchRequest)
        } catch let err {
            fatalError("Error: \(err)")
        }
        return arrCompany
    }
    
    func createEmployee(name:String,
                        company:Company,
                        born: Date,
                        position:String ) -> (Employee?, Error?) {
        let emp = NSEntityDescription.insertNewObject(forEntityName: "Employee",
                                                    into: CoreDataService.context) as! Employee
        emp.setValue(name, forKey: "name")
        emp.setValue(company, forKey: "company")
        emp.relationEmployeeInformation?.dateofbirth = born
        emp.position = position
        let employeeInfo = NSEntityDescription.insertNewObject(forEntityName: "EmployeeInformation", into: CoreDataService.context) as! EmployeeInformation
 
        employeeInfo.dateofbirth = Date()
        employeeInfo.taxid = "568119876"
        
        emp.relationEmployeeInformation = employeeInfo
    //      let company = Company()
    
        do {
         try CoreDataService.context.save()
            return (emp, nil)
        } catch let err {
            print("Error: \(err)")
            return (nil, err)
        }
    }
}


