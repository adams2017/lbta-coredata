//
//  CompaniesController+TableViewDelegate.swift
//  lbta-coredata
//
//  Created by Admin on 5/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

extension CompaniesController {
    //MARK: TABLE VIEW ROWS
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
        let ec = EmployeesController()
        ec.company = companies[indexPath.row]
        navigationController?.pushViewController(ec, animated: true )
    
    
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CompanyCell
        let company = companies[indexPath.row]
        c.aCompany = company
        return c
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (_, indexPath) in
            //  1) delete from the environment
            CoreDataService.context.delete(self.companies[indexPath.row])
            //  2) persist environment
            do {
                try CoreDataService.context.save()
            } catch let err {
                print("Error: \(err)")
            }
            // 3) delete it from the data source using the integer ROW #
            self.companies.remove(at: indexPath.row)
            // 4) deleteit from the table view using the INDEX PATH
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
        delete.backgroundColor = UIColor.red
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (_, indexPath) in
            let editController = CreateCompanyController()
            editController.aCompany = self.companies[indexPath.row]
            editController.delegate = self
            let nc = LightStatusNC(rootViewController: editController)
            self.present(nc, animated: true, completion: nil)
        }
        edit.backgroundColor = UIColor.blue
        return [delete,edit]  // return the actions and button names/styles
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return noCompaniesLabel
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return companies.count == 0 ? 150:0
    }
}
