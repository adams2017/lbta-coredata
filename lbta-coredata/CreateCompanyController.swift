//
//  CreateCompanyController.swift
//  lbta-coredata
//
//  Created by Admin on 5/19/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit
import CoreData
protocol CreateCompanyDelegate {
    func addCompany(_ commpany:Company)
    func didEditCompany(_ company:Company)
}

class CreateCompanyController: UIViewController,
      UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    var delegate:CreateCompanyDelegate?
    
    var aCompany:Company? {
        didSet {
            self.nameTextField.text = self.aCompany?.name
            if let dateFounded = self.aCompany?.founded {
                datePicker.date = dateFounded
            } //otherwise it will default to today
            if let imageData = self.aCompany?.imageData {
                self.companyImageView.image = UIImage(data: imageData)
                companyImageView.layer.cornerRadius = companyImageView.bounds.width/2
                companyImageView.clipsToBounds = true
                companyImageView.layer.borderWidth = 3
                companyImageView.layer.borderColor = UIColor.darkGray.cgColor
            }
        }
    }
    
    override func viewDidLoad() {
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = self.aCompany == nil ? "Create Company" : "Edit Company"
    }
    @objc func  doneSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; print("\n❌ Line \(#line) func \(#function) \(file)\n")
        dismiss(animated: true, completion: nil)
    }
    @objc func saveSelector() {
        if aCompany == nil {
            saveNewCompany()
        } else {
            saveExistingCompany()
        }
    }
    
    func saveNewCompany() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ Line \(#line) func \(#function) \(file)\n")
        
        #warning("snippet")
        let company = NSEntityDescription.insertNewObject(forEntityName: "Company", into: CoreDataService.context)
        company.setValue(nameTextField.text, forKey: "name")
        company.setValue(datePicker.date, forKey: "founded")
        
        // get image
        if let  imageData = companyImageView.image?.pngData() {
            company.setValue(imageData, forKey: "imageData")
        }
        
        do {
            try CoreDataService.context.save()
        } catch let err {
            print("context.save Error: \(err)")
        }
        self.delegate?.addCompany(company as! Company)
        dismiss(animated: true, completion: nil )
        
    }
    
    func saveExistingCompany() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ Line \(#line) func \(#function) \(file)\n")
        
        aCompany?.name = self.nameTextField.text
        aCompany?.founded = self.datePicker.date
        if let  image = companyImageView.image {
            let imageData = image.jpegData(compressionQuality: 0.8)
            aCompany?.setValue(imageData, forKey: "imageData")
            let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
        }
        
        do {
            try CoreDataService.context.save()
        } catch let err {
            print("Error: \(err)")
        }
  
        dismiss(animated: true) {
             // inform Companies controller of changes
            self.delegate?.didEditCompany(self.aCompany!)
        }
    }
    
    
    func setupUI() {
        navigationItem.leftBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(doneSelector))
        
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveSelector))
        
        [lightBlueBackgroundView, nameLabel, nameTextField,datePicker, companyImageView].forEach {
            view.addSubview($0); $0.translatesAutoresizingMaskIntoConstraints=false}

        let lightBlueHeight:CGFloat = 350
        NSLayoutConstraint.activate([
            lightBlueBackgroundView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            lightBlueBackgroundView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            lightBlueBackgroundView.heightAnchor.constraint(equalToConstant: lightBlueHeight),
            lightBlueBackgroundView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            
            companyImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8),
            companyImageView.heightAnchor.constraint(equalToConstant: 100),
            companyImageView.widthAnchor.constraint(equalToConstant: 100),
            companyImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            nameLabel.topAnchor.constraint(equalTo: companyImageView.bottomAnchor, constant: 4),
            nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            nameLabel.heightAnchor.constraint(equalToConstant: 50),
            nameLabel.widthAnchor.constraint(equalToConstant: 100),
            
            nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor, constant: 0),
            nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor),
            nameTextField.bottomAnchor.constraint(equalTo: nameLabel.bottomAnchor),
            nameTextField.rightAnchor.constraint(equalTo: view.rightAnchor),
            
            datePicker.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
            datePicker.leftAnchor.constraint(equalTo: nameLabel.leftAnchor, constant:   0),
            datePicker.bottomAnchor.constraint(equalTo: lightBlueBackgroundView.bottomAnchor, constant: 0),
            datePicker.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            ])

    }
    
    let nameLabel:UILabel = {
        let ui = UILabel()
        ui.text = "Name"
        ui.font = UIFont.systemFont(ofSize: 16 )
        ui.textColor = UIColor.black
        ui.backgroundColor = UIColor.clear
        ui.numberOfLines = 1
        return ui
    }()
    let nameTextField:UITextField  = {
        let ui = UITextField()
        ui.placeholder = "Name here"
       // ui.backgroundColor = UIColor.white
        ui.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        ui.leftViewMode = UITextField.ViewMode.always
        ui.layer.borderColor = UIColor.lightGray.cgColor
        ui.layer.borderWidth = 1
        ui.layer.cornerRadius = 16
        return ui
    }()
    
    let lightBlueBackgroundView:UIView = {
        let ui = UIView()
        ui.backgroundColor = UIColor.cyan
        return ui
    }()
    
    let datePicker:UIDatePicker = {
        let ui = UIDatePicker()
        ui.datePickerMode = .date
        return ui
    }()
    lazy var companyImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = #imageLiteral(resourceName: "iinfo")
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        ui.isUserInteractionEnabled = true
        ui.addGestureRecognizer(
            UITapGestureRecognizer(target: self,
            action: #selector(companyImageTapGestureSelector)))
        return ui
    }()
    
 
    
    @objc func companyImageTapGestureSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
        present(imagePickerController, animated: true, completion: nil)
        // dismiss is done in one of the 2 delegates
    }
    
    // plist: privacy - access photo library 
    lazy var imagePickerController:UIImagePickerController =  {
        let ui = UIImagePickerController()
        ui.delegate = self //
        ui.allowsEditing = true
        return ui
    }()
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let aImage = info[UIImagePickerController.InfoKey.editedImage] as?  UIImage {
            companyImageView.image = aImage
        } else if let aImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            companyImageView.image = aImage
        }
        companyImageView.layer.cornerRadius = companyImageView.bounds.width/2
        companyImageView.clipsToBounds = true
        companyImageView.layer.borderWidth = 3
        companyImageView.layer.borderColor = UIColor.darkGray.cgColor
        dismiss(animated: true, completion: nil)
    }
    
 
    
    

}

