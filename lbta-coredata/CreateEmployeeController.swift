//
//  CreateEmployeeController.swift
//  lbta-coredata
//
//  Created by Admin on 5/27/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class CreateEmployeeController:UIViewController {
    var aCompany:Company?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    func setup() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) Line \(#line) of \(file)\n")
        view.backgroundColor = UIColor.green
        navigationItem.title = (aCompany?.name ?? "") + " Employee"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelSelector))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveSelector))
        layoutViews()
    }
        
    @objc private func cancelSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) @ Line \(#line) of \(file)\n")
        dismiss(animated: true, completion: nil)
    }

    
    @objc private func saveSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) @ Line \(#line) of \(file)\n")
        guard let name = nameTextField.text else { return }
        guard let born = bornTextField.text else { return }
        
        // edit form
        if born.isEmpty {
            let alert = UIAlertController(
                title: "Alert",
                message: "You must enter date of birth! ",
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true )
            return
        }
        
        if name.isEmpty {
            let alert = UIAlertController(
                    title: "Alert!",
                    message: "Name must be entered!",
                    preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true )
            return
        }
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "M/d/y"
        let bornDate = formatter.date(from: born )
        if bornDate == nil {
            let alert = UIAlertController(
                title: "ALERT!!!",
                message: "BAD DATE ",
                preferredStyle: .alert)
            alert.addAction( UIAlertAction(title:"CANCEL", style: .cancel))
            alert.addAction(UIAlertAction(title: "DEFAULT ", style: .default))
            self.present(alert, animated: true )
            return
        }

        let ix = positionSegmentedControl.selectedSegmentIndex
        guard let position = positionSegmentedControl.titleForSegment(at: ix) else { return}

        let resultTuple =  CoreDataService.shared.createEmployee(
                            name: name,
                            company: aCompany!,
                            born:  bornDate!,
                            position: position)
        
        if resultTuple.1 != nil {
            print(resultTuple.1.debugDescription)
        }
        dismiss(animated: true, completion: nil)
        /*
        dismiss(animated: true) {
            //Brian calls a delegate to add the employee to the employees after the animation completes
        }
        */
    }
    
    func layoutViews()  {
        
        [lightBlueBackgroundView, nameLabel, nameTextField, bornLabel, bornTextField,
         positionSegmentedControl].forEach {
            view.addSubview($0); $0.translatesAutoresizingMaskIntoConstraints=false}
        
        let lightBlueHeight:CGFloat = 200
        NSLayoutConstraint.activate([
            lightBlueBackgroundView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            lightBlueBackgroundView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            lightBlueBackgroundView.heightAnchor.constraint(equalToConstant: lightBlueHeight),
            lightBlueBackgroundView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            
            nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            nameLabel.heightAnchor.constraint(equalToConstant: 50),
            nameLabel.widthAnchor.constraint(equalToConstant: 100),
            
            nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor, constant: 0),
            nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor, constant: 0),
            nameTextField.bottomAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 0),
            nameTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8),
            
            bornLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 16),
            bornLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor, constant: 0),
            bornLabel.heightAnchor.constraint(equalToConstant: 50),
            bornLabel.widthAnchor.constraint(equalToConstant: 100),
            
            bornTextField.topAnchor.constraint(equalTo: bornLabel.topAnchor, constant: 0),
            bornTextField.leftAnchor.constraint(equalTo: nameTextField.leftAnchor, constant: 0),
            bornTextField.heightAnchor.constraint(equalToConstant: 50),
            bornTextField.rightAnchor.constraint(equalTo: nameTextField.rightAnchor, constant: 0),
            
            positionSegmentedControl.topAnchor.constraint(equalTo: bornLabel.bottomAnchor, constant: 8),
            positionSegmentedControl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            positionSegmentedControl.heightAnchor.constraint(equalToConstant: 35),
            positionSegmentedControl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8)

            
            
            ])
        
    }
    
    let lightBlueBackgroundView:UIView = {
        let ui = UIView()
        ui.backgroundColor = UIColor.cyan
        return ui
    }()
    
    let nameLabel:UILabel = {
        let ui = UILabel()
        ui.text = "Name"
        ui.font = UIFont.boldSystemFont(ofSize: 16 )
        ui.textColor = UIColor.black
        ui.backgroundColor = UIColor.clear
        ui.numberOfLines = 1
        return ui
    }()
    let nameTextField:UITextField  = {
        let ui = UITextField()
        ui.placeholder = "Name here"
        // ui.backgroundColor = UIColor.white
        ui.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        ui.leftViewMode = UITextField.ViewMode.always
        ui.layer.borderColor = UIColor.lightGray.cgColor
        ui.layer.borderWidth = 1
        ui.layer.cornerRadius = 16
        return ui
    }()
    
    let bornLabel:UILabel = {
        let ui = UILabel()
        ui.text = "Born"
        ui.font = UIFont.boldSystemFont(ofSize: 16)
        ui.textColor = UIColor.black
        ui.backgroundColor = UIColor.clear
        ui.numberOfLines = 1
        return ui
    }()
    let bornTextField:UITextField  = {
        let ui = UITextField()
        ui.placeholder = "MM/DD/YY"
        ui.backgroundColor = UIColor.clear
        ui.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        ui.leftViewMode = UITextField.ViewMode.always
        ui.layer.borderColor = UIColor.lightGray.cgColor
        ui.layer.borderWidth = 1
        ui.layer.cornerRadius = 16
        return ui
    }()
    
    lazy var positionSegmentedControl:UISegmentedControl = {
        var positions = [String]()
        _=CompanyPosition.allCases.map {positions.append($0.rawValue) }
        let ui = UISegmentedControl(items: positions)
        ui.selectedSegmentIndex = 0
        ui.tintColor = UIColor.blue
        return ui
    }()

}

