//
//  CompanyCell.swift
//  lbta-coredata
//
//  Created by Admin on 5/26/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class CompanyCell:UITableViewCell {
    
    var aCompany:Company? {
        didSet {
            guard let aCompany = aCompany  else { return }
            if let imageData = aCompany.imageData {
                companyImageView.image = UIImage(data: imageData)
            }
            var foundedDateText = ""
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy"

            if let founded = aCompany.founded {
                print(founded)
                foundedDateText = formatter.string(from: founded )
                print(foundedDateText)
            }
           // nameFoundedDateLabel.text = aCompany.name ?? "<No Name>" + " Founded: " + foundedDateText
          
            nameFoundedDateLabel.text = "\(aCompany.name ?? "") Founded: \(foundedDateText) "
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super .init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    func setup() {
        self.backgroundColor = .red
        layoutViews()
    }
    
    func layoutViews()  {
        [companyImageView, nameFoundedDateLabel].forEach {
            self.addSubview($0); $0.translatesAutoresizingMaskIntoConstraints=false}
 
        NSLayoutConstraint.activate([
           
           companyImageView.widthAnchor.constraint(equalToConstant: 40),
           companyImageView.heightAnchor.constraint(equalToConstant: 40),
           companyImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
           companyImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
           
           nameFoundedDateLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
           nameFoundedDateLabel.leftAnchor.constraint(equalTo: self.companyImageView.rightAnchor, constant: 8),
           nameFoundedDateLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
           nameFoundedDateLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0)
        ])
    }
    
    
    let companyImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = #imageLiteral(resourceName: "iinfo")
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        ui.layer.cornerRadius = 20
        ui.layer.borderWidth = 2
        ui.layer.borderColor = UIColor.blue.cgColor
        return ui
    }()

    let nameFoundedDateLabel:UILabel = {
        let ui = UILabel()
        ui.text = "<Founded>"
        ui.font = UIFont.boldSystemFont(ofSize: 16)
        ui.textColor = UIColor.white
        ui.backgroundColor = UIColor.clear
        ui.numberOfLines = 1
        return ui
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
