//
//  ViewController.swift
//  lbta-coredata
//
//  Created by Admin on 5/17/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

// 5/19. lesson 3 done
// 4 Done
// 5/20: 5 done
// 5/20: 6 done
// 5/20: 7 done
// 5/24: 10 done
// 5/24: 11 done
// 5/25: 12 done, 13, 14 , 15
// 5/27: 16,17
// 5/28: 18,19
// 5/29: 20
// 5/30: 23

import UIKit
import CoreData

class CompaniesController: UITableViewController {
 
    let myTheme = (UIApplication.shared.delegate as! AppDelegate).theme
    var companies = [Company]()
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.companies = CoreDataService.shared.fetchCompanies() // table view will automatically reload itself
        setupViews()
        setupNav()
        setupTableView()
      //  let x = UIColor.Tyre.superSoft
    }
    
    func setupTableView() {
        #warning("snippet")
        tableView.backgroundColor = myTheme.color(for: .footer)
        tableView.register(CompanyCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorColor = .white
        tableView.tableFooterView = UIView()

    }
    func setupViews() {
        #warning("snippet?")
        view.backgroundColor = .white
       // view.backgroundColor = Theme.summer.color(for: .footer)
       // view.backgroundColor = Theme.summer.element(element: <#T##Theme.Element#>)
    }
    func setupNav() {
        setupNavStyle()
        setupNavButtons()
        setupTableView()
    }
 
    #warning("add to snippets")
    func setupNavStyle() {
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.title = "Companies"
    }
 
    func setupNavButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus"), style: .plain, target: self, action: #selector(handleAddCompany))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(resetSelector))
    }
    
    @objc private func resetSelector() {
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ \(#function) @ Line \(#line) of \(file)\n")
        #warning("snippets")
        /*
        //Method 1: A loop
       _=companies.map { CoreDataService.context.delete($0)}
        do {
            try CoreDataService.context.save()
        } catch let err {
            fatalError("Error: \(err)")
        }
        companies.removeAll()
        tableView.reloadData()
*/
        //Method 2 :
        
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Company.fetchRequest() )
        do {
            try CoreDataService.context.execute(batchDeleteRequest)
            companies.removeAll()
            tableView.reloadData()
        } catch let err {
            print("Error: \(err)")
        }
        
    }
    @objc func handleAddCompany() {
    
      //  let file = "\(#file)".components(separatedBy: "/").last!
        //NSLog("\n❌ Line \(#line) func \(#function) \(file)\n")
        let file = "\(#file)".components(separatedBy: "/").last!; NSLog("\n❌ Line \(#line) func \(#function) \(file)\n")
      
        let createCompanyController = CreateCompanyController()
        createCompanyController.delegate = self
        let nc = LightStatusNC(rootViewController: createCompanyController)
        present(nc, animated: true, completion: nil)
    }

    let noCompaniesLabel:UILabel = {
        let ui = UILabel()
        ui.text = "No Companies !!!"
        ui.font = UIFont.systemFont(ofSize: 20)
        ui.textColor = UIColor.black
        ui.textAlignment = .center 
        //ui.backgroundColor = UIColor.white
        ui.numberOfLines = 1
        return ui
    }()
}


