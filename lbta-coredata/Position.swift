//
//  Position.swift
//  lbta-coredata
//
//  Created by Admin on 5/30/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation

enum CompanyPosition:String,CaseIterable {
    case Executive
    case SeniorManagement = "Senior Management"
    case Staff
    case Intern
}
